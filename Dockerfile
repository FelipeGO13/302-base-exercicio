FROM openjdk:8-alpine
WORKDIR /app
COPY /target/Api-Investimentos-*.jar .
CMD ["java", "-jar", "Api-Investimentos-0.0.1-SNAPSHOT.jar"]
